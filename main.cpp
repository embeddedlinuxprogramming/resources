#include <iostream>
#include <thread>
#include <chrono>

#include "CPUUsage.h"
#include "MemUsage.h"

using std::cout;
using std::thread;
using namespace std::chrono_literals;

int main() {
    while (true) {
        //read cpu usage.
        cout << "CPU usage: " << CPUUsage() << " %" ;
        // read memory usage.
        cout << "     Memory usage: " << MemUsage() << " %" << std::endl;
        std::this_thread::sleep_for(2000ms);
    }
    return 0;
}
