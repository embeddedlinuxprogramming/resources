//
// Created by jbgomezm on 15/02/24.
//

#include <fstream>
#include <map>
#include <cstring>

using std::ifstream;
using std::map;
using std::string;

double MemUsage() {
    // Open /proc/meminfo
    ifstream meminfoFile;
    meminfoFile.open("/proc/meminfo");
    if (!meminfoFile.fail()) {
        map<string, unsigned long> meminfo;
        string param, discard;
        unsigned long value;
        // Read memory parameters up to swap free.
        while (strcmp("SwapFree:", param.data()) != 0) {
            meminfoFile >> param >> value >> discard;
            meminfo[param.data()] = value;
        }
        meminfoFile.close();
        // Compute percentage
        double perc = 100 - double(100*meminfo["MemFree:"])/double(meminfo["MemTotal:"]);
        return perc;
    }
    else return -1;
}
