//
// Created by jbgomezm on 15/02/24.
//

#include <fstream>
#include <map>
#include <cstring>

using std::ifstream;
using std::map;
using std::string;

double CPUUsage() {
    static unsigned long lastTotal = 0, lastTotalIdle = 0;
    // Open /proc/stat
    ifstream cpuinfoFile;
    cpuinfoFile.open("/proc/stat");
    if (!cpuinfoFile.fail()) {
        string discard;
        unsigned long totalUser, totalUserLow, totalSys, totalIdle;
        // Read CPU parameters.
        cpuinfoFile >> discard >> totalUser >> totalUserLow >> totalSys >> totalIdle;
        cpuinfoFile.close();
        // Compute percentage
        lastTotal = totalUser + totalUserLow + totalSys + totalIdle - lastTotal;
        lastTotalIdle = totalIdle - lastTotalIdle;
        double perc = 100 - double(100*(lastTotalIdle))/double(lastTotal);
        return perc;
    }
    else return -1;
}