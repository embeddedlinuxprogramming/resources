# Resources
This project aims to showcase the use of files and maps to read the resource utilization of the machine, namely the 
memory usage and CPU load. Both variables are read once each 2 seconds.

___Written by Juan Bernardo Gómez Mendoza.<br>
Latest update: 2024/02/15.___

---

## Memory information gathering
Memory related information is read from the `/proc/meminfo` 
file.

The typical head (first lines) of the file looks like this

    MemTotal:       65621584 kB
    MemFree:        50151592 kB
    MemAvailable:   58374000 kB
    Buffers:          284048 kB
    ...

The file is loaded using the following code

    ...
    ifstream meminfoFile;
    meminfoFile.open("/proc/meminfo");
    if (!meminfoFile.fail()) {
        map<string, unsigned long> meminfo;
        string param, discard;
        unsigned long value;
        // Read memory parameters up to swap free.
        while (strcmp("SwapFree:", param.data()) !=0) {
            meminfoFile >> param >> value >> discard;
            meminfo[param.data()] = value;
        }
        meminfoFile.close();
    ...

In the code, we load all the lines contained in `/proc/meminfo` up to the one starting with `SwapFree:` inside a map, 
where the map key is the string contained in the first column of the file, and the value is the amount of memory 
in `kB`. 

Although unnecessary, reading all those fields enables the programmer to include other useful information such
as cached memory and virtual memory utilization.

---

## CPU usage information gathering
CPU usage information can be acquired by reading the file `/proc/stat`, as in
[this post from Stack Overflow](https://stackoverflow.com/questions/63166/how-to-determine-cpu-and-memory-consumption-from-inside-a-process/).

The head of the file will look something like that (assuming the current processor has at least 2 virtual or physical 
CPUs):

    cpu  118406 159233 34530 2313815 19054 0 3054 0 0 0
    cpu0 7017 9438 2100 145943 721 0 116 0 0 0
    cpu1 7476 9418 1863 143909 2656 0 194 0 0 0
    cpu2 7043 12232 1966 142935 973 0 290 0 0 0
    ...

Using the information contained in the first line, it is possible to compute the CPU usage. The following code performs
both the file loading and the CPU usage calculation.

    ...
    static unsigned long lastTotal = 0, lastTotalIdle = 0;
    // Open /proc/stat
    ifstream cpuinfoFile;
    cpuinfoFile.open("/proc/stat");
    if (!cpuinfoFile.fail()) {
        string discard;
        unsigned long totalUser, totalUserLow, totalSys, totalIdle;
        // Read CPU parameters.
        cpuinfoFile >> discard >> totalUser >> totalUserLow >> totalSys >> totalIdle;
        cpuinfoFile.close();
        // Compute percentage
        lastTotal = totalUser + totalUserLow + totalSys + totalIdle - lastTotal;
        lastTotalIdle = totalIdle - lastTotalIdle;
        double perc = 100 - double(100*(lastTotalIdle))/double(lastTotal);
        return perc;
    }
    ...
